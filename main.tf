# Configure the AWS Provider
provider "aws" {
  region = "us-east-2"

}

resource "aws_s3_bucket" "leo19128" {
  bucket = var.bucket_name

  tags = {
    Name        = var.bucket_name
    Environment = "Dev"
  }
}