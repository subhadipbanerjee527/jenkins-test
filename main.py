import datetime
import smtplib
import os
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from docx import Document

today = datetime.datetime.now()

doc = Document("Decillion consultants Timesheet.docx")
# with open('Decillion consultants Timesheet.xml', 'w') as f:
# 	f.write(doc._element.xml)

par = doc.paragraphs[2]

day = par.runs[9]
day.text = str(today.day)

month = par.runs[12]
month.text = today.strftime("%B")

dname = "Decillion consultants Timesheet "+str(today.day)+"."+str(today.month)+"."+str(today.year)+".docx"
doc.save(dname)

print(os.environ['APP_EMAIL'])
print(os.environ)
mail_content = '''Hi Nachiket,
PFA
Thanks
Subhadip'''
#The mail addresses and password
sender_address = 'subhadipbanerjee527@gmail.com'
sender_pass = os.environ["APP_EMAIL"]
receiver_address = 'sblinkinparkjee@gmail.com'
#Setup the MIME
message = MIMEMultipart()
message['From'] = sender_address
message['To'] = receiver_address
message['Subject'] = 'Weekly Report '+str(today.day)+"."+str(today.month)+"."+str(today.year)   #The subject line
#The body and the attachments for the mail
message.attach(MIMEText(mail_content, 'plain'))
with open(dname, "rb") as fil:
    part = MIMEApplication(
        fil.read(),
        Name=dname
    )
# Open the file as binary mode
part['Content-Disposition'] = 'attachment; filename="%s"' % dname
message.attach(part)

#Create SMTP session for sending the mail
session = smtplib.SMTP('smtp.gmail.com', 587) #use gmail with port
session.starttls() #enable security
session.login(sender_address, sender_pass) #login with mail_id and password
text = message.as_string()
session.sendmail(sender_address, receiver_address, text)
session.quit()
print('Mail Sent')